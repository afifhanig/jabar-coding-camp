
//Soal No. 1 --------------------------
console.log("Soal No. 1 ------")
var tanggal = 29
var bulan = 2
var tahun = 2020

console.log("Besoknya : " + next_date(tanggal, bulan, tahun))

function next_date(tgl, bln, thn) {
    console.log("Inputan tanggal bulan dan tahun : " + tgl, bln, thn)

    var newTgl, newBln, newThn, month

    if (thn % 400 == 0 || thn % 4 == 0) {
        console.log("Tahun Kabisat!!")
        if (bln == 2) {
            if (tgl == 29) {
                newTgl = 1;
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 29) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 29"
            }
            else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 1 || bln == 3 || bln == 5 || bln == 7 || bln == 9 || bln == 11) {
            if (tgl == 31) {
                newTgl = 1
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 31) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 31"
            }
            else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 4 || bln == 6 || bln == 8 || bln == 10) {
            if (tgl == 30) {
                newTgl = 1
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 30) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 30"
            } else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 12) {
            if (tgl == 31) {
                newTgl = 1
                newBln = 1
                newThn = thn + 1
            } else if (tgl > 31) {
                return "--Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 31"
            } else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln > 12) {
            return " --Warning!!-- Maksimal Bulan adalah 12"
        }
    } else {
        if (bln == 2) {
            if (tgl == 28) {
                newTgl = 1;
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 28) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 28"
            }
            else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 1 || bln == 3 || bln == 5 || bln == 7 || bln == 9 || bln == 11) {
            if (tgl == 31) {
                newTgl = 1
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 31) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 31"
            }
            else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 4 || bln == 6 || bln == 8 || bln == 10) {
            if (tgl == 30) {
                newTgl = 1
                newBln = bln + 1
                newThn = thn
            } else if (tgl > 30) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 30"
            }
            else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln == 12) {
            if (tgl == 31) {
                newTgl = 1
                newBln = 1
                newThn = thn + 1
            } else if (tgl > 31) {
                return " --Warning!!-- Maksimal Tanggal Untuk Bulan Ini Adalah 31"
            } else {
                newTgl = tgl + 1
                newBln = bln
                newThn = thn
            }
        } else if (bln >= 12) {
            return " --Warning!!-- Maksimal Bulan adalah 12"
        }

    }

    switch (newBln) {
        case 1: month = "Januari"; break;
        case 2: month = "Februari"; break;
        case 3: month = "Maret "; break;
        case 4: month = "April "; break;
        case 5: month = "Mei"; break;
        case 6: month = "Juni"; break;
        case 7: month = "Juli"; break;
        case 8: month = "Agustus"; break;
        case 9: month = "September"; break;
        case 10: month = "Oktober"; break;
        case 11: month = "November"; break;
        case 12: month = "Desember"; break;
    }
    return newTgl + " " + month + " " + newThn
}
//Soal No. 1 ---------------------------
console.log("")
//Soal No. 2 --------------------------
console.log("Soal No. 2 ------")
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

console.log("Jumlah Kata Pada Kalimat 1 : " + jumlah_kata(kalimat_1))
console.log("Jumlah Kata Pada Kalimat 2 : " + jumlah_kata(kalimat_2))
console.log("Jumlah Kata Pada Kalimat 3 : " + jumlah_kata(kalimat_3))

function jumlah_kata(kalimat) {
    var newSplits = []
    var splits = kalimat.split(" ")
    for (var i = 0; i < splits.length; i++) {
        if (splits[i] == "") {
            continue
        } else {
            newSplits.push(splits[i])
        }
    }
    //console.log(newSplits)
    return newSplits.length
}
//Soal No. 2 --------------------------

