console.log("Soal No. 1")
var pertama = "saya senang belajar "
var kedua = "JAVASCRIPT";
console.log(pertama + kedua) //Jawaban Soal No. 1
console.log("------")

console.log("Soal No. 2")
var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"

var intKataPertama = parseInt(kataPertama)
var intKataKedua = parseInt(kataKedua)
var intKataTiga = parseInt(kataKetiga)
var intKataKeempat = parseInt(kataKeempat)

var hasil = (intKataPertama + intKataKedua) * (intKataKeempat - intKataTiga)
console.log("Jawabannya : " + hasil) //Jawaban Soal No. 2
console.log("------")

//Soal No 3
console.log("Soal No. 3")
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14) //Jawaban Soal No. 3
var kataKetiga = kalimat.substring(15, 18) //Jawaban Soal No. 3
var kataKeempat = kalimat.substring(19, 25) //Jawaban Soal No. 3
var kataKelima = kalimat.substring(25, 32) //Jawaban Soal No. 3

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
console.log("------")