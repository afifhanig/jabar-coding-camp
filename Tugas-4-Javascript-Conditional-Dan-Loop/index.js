
//Soal No. 1 -----------------------------------
console.log('Soal No. 1')
var nilai = 75
if (nilai > 100) {
    console.log("Nilai tidak bisa diatas 100")
} else if (nilai < 0) {
    console.log("Nilai tidak angka negatif")
} else {
    if (nilai >= 85) {
        console.log("indeksnya A")
    } else if (nilai >= 75 && nilai < 85) {
        console.log("indeksnya B")
    } else if (nilai >= 65 && nilai < 75) {
        console.log("indeksnya C")
    } else if (nilai >= 55 && nilai < 65) {
        console.log("indeksnya D")
    } else {
        console.log("indeksnya E")
    }
}
//Soal No. 1 -----------------------------------

console.log('')

//Soal No. 2 -----------------------------------
console.log('Soal No. 2')
var tanggal = 3;
var bulan = 7;
var tahun = 1997;
var month = '';

switch (bulan) {
    case 1: month = "Januari"; break;
    case 2: month = "Februari"; break;
    case 3: month = "Maret "; break;
    case 4: month = "April "; break;
    case 5: month = "Mei"; break;
    case 6: month = "Juni"; break;
    case 7: month = "Juli"; break;
    case 8: month = "Agustus"; break;
    case 9: month = "September"; break;
    case 10: month = "Oktober"; break;
    case 11: month = "November"; break;
    case 12: month = "Desember"; break;
}
console.log(tanggal + " " + month + " " + tahun);
//Soal No. 2 -----------------------------------

console.log('')

//Soal No. 3 -----------------------------------
console.log("Soal No. 3")
var tangga = 3;
for (var i = 0; i < tangga; i++) {
    process.stdout.write("#");

    for (var j = 0; j < i; j++) {
        process.stdout.write("#");
    }
    console.log("");
}
//Soal No. 3 -----------------------------------

console.log('')

//Soal No.4  -----------------------------------
console.log('Soal No. 4')
var j = 0;
for (var angka = 0; angka < 10; angka++) {

    if (angka % 3 == 0) {
        j = 0
        var s = "";
        for (var strip = 1; strip <= angka; strip++) {
            s += "=";
        }
        console.log(s);
    }

    if (j == 0) {
        console.log((angka + 1) + ' - I love programming')
    } else if (j == 1) {
        console.log((angka + 1) + ' - I love Javascript')
    } else if (j == 2) {
        console.log((angka + 1) + ' - I love VueJS')
    }
    j++

}
//Soal No.4  -----------------------------------








