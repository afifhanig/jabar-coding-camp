//Soal No. 1 ----------------
console.log("Soal No. 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i = 0; i < daftarHewan.length; i++) {

    for (var j = i + 1; j < daftarHewan.length; j++) {
        var temp = 0;
        if (daftarHewan[j] < daftarHewan[i]) {
            temp = daftarHewan[i];
            daftarHewan[i] = daftarHewan[j];
            daftarHewan[j] = temp;
        }
    }

    console.log(daftarHewan[i] + " ");
}
//Soal No. 1 ----------------

console.log('')

//Soal No. 2 ----------------
console.log("Soal No. 2");
function introduce(data) {
    var kalimat = "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby + " ";
    return kalimat
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)
//Soal No. 2 ----------------

console.log('')

//Soal No. 3 ----------------
console.log("Soal No. 3")
function hitung_huruf_vokal(kata) {
    var a = 0, b = 0, c = 0, d = 0, e = 0

    hurufkecil = kata.toLowerCase()
    for (var x = 0; x < kata.length; x++) {
        if (hurufkecil.charAt(x) == 'a') {
            a++;
        }
        if (hurufkecil.charAt(x) == 'i') {
            b++;
        }
        if (hurufkecil.charAt(x) == 'u') {
            c++;
        }
        if (hurufkecil.charAt(x) == 'e') {
            d++;
        }
        if (hurufkecil.charAt(x) == 'o') {
            e++;
        }
    }
    var jumlahHrfVokal = a + b + c + d + e
    return jumlahHrfVokal
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2) // 3 2
//Soal No. 3 ----------------

console.log('')

//Soal No. 4 ----------------
console.log("Soal No. 4")
function hitung(x) {
    var angka = []
    for (var i = -2; i <= 8; i++) {
        if (i % 2 == 0) {
            angka.push(i)
        }
    }
    return angka[x]
}
console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))
//Soal No. 4 ----------------
