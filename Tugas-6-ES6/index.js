
console.log("")
//Soal No. 1 ------
console.log("Soal No. 1")

const luasDanKelilingPersegiPanjang = (panjang, lebar) => {
    let luas, keliling
    const two = 2

    luas = panjang * lebar
    keliling = two * (panjang + lebar)

    console.log("Luasnya : " + luas)
    console.log("Kelilingnya : " + keliling)
}

luasDanKelilingPersegiPanjang(5, 2)
//Soal No. 1 ------
console.log("")
//Soal No. 2 ------
console.log("Soal No. 2")
const newFunction = function literal(firstName, lastName) {
    return {
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }
}
newFunction("William", "Imoh").fullName()
//Soal No. 2 ------

console.log("")
//Soal No. 3 ------
console.log("Soal No. 3")
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)
//Soal No. 3 ------

console.log("")
//Soal No. 4 ------
console.log("Soal No. 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
console.log(combinedArray)
//Soal No. 4 ------

console.log("")
//Soal No. 5 ------
console.log("Soal No. 5")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`
console.log(before)
//Soal No. 5 ------
console.log("")

